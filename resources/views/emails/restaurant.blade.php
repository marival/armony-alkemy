<table width="600px" border="0" cellspacing="2" cellpadding="5" style="font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; color:#4A4A4A">
   <tr>
      <td align="center">
        {{-- LOGO --}}
      </td>
      <td>
         <h3>Restaurante</h3>
         <h3>{{ $data->resturant }}</h3>
         <br><strong>Tel. 52 (322) 297-0262</strong> Av. Paseo Cocoteros Lote 53 Villa 8 11 Nuevo Vallarta, Nayarit, 63732 México
      </td>
   </tr>
   <tr>
      <td><strong>Nombre</strong></td>
      <td>{{ $data->name }}</td>
   </tr>
   <tr bgcolor="#eff0f2">
      <td><strong>Apellido</strong></td>
      <td>{{ $data->last_name }}</td>
   </tr>
   <td><strong>Nombre de Restaurante</strong></td>
   <td>{{ $data->restaurant }}</td>
   </tr>
   <tr bgcolor="#eff0f2">
      <td><strong>phone</strong></td>
      <td>{{ $data->phone }}</td>
   </tr>
   <tr>
      <td><strong>Email</strong></td>
      <td>{{ $data->email }}</td>
   </tr>
   <tr bgcolor="#eff0f2">
      <td><strong>No. Reservación</strong></td>
      <td>{{ $data->num_reservacion }}</td>
   </tr>
   <tr>
      <td><strong>No. Personas</strong></td>
      <td>{{ $data->total_personas }}</td>
   </tr>
   <tr bgcolor="#eff0f2">
      <td><strong>Fecha de reservación</strong></td>
      <td>{{ $data->reservation_date }}</td>
   </tr>
   <tr>
      <td><strong>Hora de reservacióna</strong></td>
      <td>{{ $data->hora_reservacion }}</td>
   </tr>
   <tr bgcolor="#eff0f2">
      <td><strong>Comentarios</strong></td>
      <td>{{ $data->more_request }}</td>
   </tr>
</table>

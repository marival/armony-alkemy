<?php

return [
    'user-already-in' => 'The email is already subscribed to this list.',
    'request-sent' => 'Thank you for subscribing to our newsletter!',
    'invalid-recaptcha' => 'The verification is not valid. Please, try again.',
    'server-failed-with-message' => 'The server could not process your request and replied with the following message: ',
];

<?php

return [
    'user-already-in' => 'El correo electrónico ya se encuentra asociado con esta lista.',
    'request-sent' => '¡Gracias por suscribirte a nuestro newsletter!',
    'invalid-recaptcha' => 'La verificación no es válida. Por favor, intenta nuevamente.',
    'server-failed-with-message' => 'El servidor no pudo procesar tu petición y respondió con el siguiente mensaje: ',
];

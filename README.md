# Marival armony

Marival armony new site

## Acknowledgements

-   [Docker docs](https://docs.docker.com/)
-   [Docker compose docs](https://docs.docker.com/compose/)
-   [Laravel docs](https://laravel.com/docs/5.8)

## Run Locally

Clone the project

```bash
  git clone git@gitlab.com:marival-group/armony2021.git
```

Go to the project directory

```bash
  cd armony2021
```

Spin up Containers

```bash
  docker-compose up -d
```

Install dependencies

```bash
  docker-compose exec app bash
  # composer install
  # exit
```

Create a .env file based on .env.example, the DB zone as follows:

```
DB_CONNECTION=mysql
DB_HOST=armony_db
DB_PORT=3306
DB_DATABASE=Armony2021
DB_USERNAME=root
DB_PASSWORD=DevOps2021
```

Run Migrations

```
docker-compose exec app php artisan migrate
```

Run seeds

```
docker-compose exec app php artisan mgcms2021:seed
```

Generate Key

```
docker-compose exec app php artisan key:generate
```

Now the project is up and ready.

Happy coding!
